export IOCNAME=VacS-ACCV:Vac-IOC-002
export IOCDIR=VacS-ACCV_Vac-IOC-002
export EPICS_DB_INCLUDE_PATH="`pwd`/db"
export EPICS_DB_INCLUDE_PATH="$(dirname ${BASH_SOURCE})/db"
export vacs_accv_vac_plc_10001_VERSION=6.0.3
