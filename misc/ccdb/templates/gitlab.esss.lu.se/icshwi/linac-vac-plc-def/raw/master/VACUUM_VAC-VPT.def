############################## Vacuum Mechanical Pump Controller ##############################
##############################           Turbo Pumps             ##############################
############################## Version: 4.0.1                    ##############################


#-############################
#- COMMAND BLOCK
#-############################

define_command_block()

add_digital("AutoCmd",                       PV_NAME="AutoModeCmd",                   PV_DESC="Set control mode to AUTOMATIC")
add_digital("ManuCmd",                       PV_NAME="ManualModeCmd",                 PV_DESC="Set control mode to MANUAL")
add_digital("StopManuCmd",                   PV_NAME="ManualStopCmd",                 PV_DESC="Manual STOP command")
add_digital("StartManuCmd",                  PV_NAME="ManualStartCmd",                PV_DESC="Manual START command")
add_digital() #ForceOnCmd
add_digital("BypassItlCmd",                  PV_NAME="OverrideITLckCmd",              PV_DESC="Override tripped interlock")
add_digital("BlockOffCmd",                   PV_NAME="AutoStartDisCmd",               PV_DESC="Disable automatic START")
add_digital("ResetCmd",                      PV_NAME="RstErrorsCmd",                  PV_DESC="Reset errors and warnings")
#-----------------------------
skip_digitals(4)
add_digital() #ClrForceOnCmd
add_digital("ClearBypassCmd",                PV_NAME="ClrOverrideITLckCmd",           PV_DESC="Clear interlock override")
add_digital("UnlockCmd",                     PV_NAME="AutoStartEnaCmd",               PV_DESC="Enable automatic START")
add_digital("ClearCountersCmd",              PV_NAME="ClrCountersCmd",                PV_DESC="Clear counters")


#-############################
#- STATUS BLOCK
#-############################

define_status_block()

add_digital("ManuSt",                        PV_NAME="ManualModeR",                   PV_DESC="Control mode status",                     PV_ONAM="Manual",              PV_ZNAM="Auto")
add_digital("OffSt",                         PV_NAME="StoppedR",                      PV_DESC="The pump is STOPPED",                     PV_ONAM="Stopped")
add_digital("AcceleratingSt",                PV_NAME="AcceleratingR",                 PV_DESC="The pump is ACCELERATING",                PV_ONAM="Accelerating")
add_digital("NominalSpeedSt",                PV_NAME="AtNominalSpdR",                 PV_DESC="The pump is running at NOMINAL SPEED",    PV_ONAM="At Nominal Speed")
add_digital("ErrorSt",                       PV_NAME="ErrorR",                        PV_DESC="Error detected by the control function",  PV_ONAM="Error", ARCHIVE=True)
skip_digitals(2)
add_digital("InterlockTriggerSt",            PV_NAME="ITLckTrigR",                    PV_DESC="Interlock triggering status",             PV_ONAM="NominalState",        PV_ZNAM="Tripped", PV_ZSV="MAJOR")
#-----------------------------
add_digital("OnDQSt",                        PV_NAME="StartDQ-RB",                    PV_DESC="Status of the START digital output",      PV_ONAM="True",                PV_ZNAM="False")
add_digital("StartManuSt",                   PV_NAME="OnManualStartR",                PV_DESC="Current START command was MANUAL",        PV_ONAM="Manual Start")
add_digital("StartAutoSt",                   PV_NAME="OnAutoStartR",                  PV_DESC="Current START command was AUTOMATIC",     PV_ONAM="Automatic Start")
add_digital("LockedSt",                      PV_NAME="AutoStartDisStR",               PV_DESC="Automatic START toggle status",           PV_ONAM="Auto Start Disabled", PV_ZNAM="Auto Start Enabled")
add_digital("WarningSt",                     PV_NAME="WarningR",                      PV_DESC="A warning is active",                     PV_ONAM="Warning")
add_digital("InvalidCommandSt",              PV_NAME="InvalidCommandR",               PV_DESC="Last command sent cannot be processed",   PV_ONAM="Invalid command")
add_digital("HardwareErrorSt",               PV_NAME="HWErrorR",                      PV_DESC="Error signal from the pump controller",   PV_ONAM="HW Error")
add_digital("ValidSt",                       PV_NAME="ValidR",                        PV_DESC="Communication is valid",                  PV_ONAM="Valid",               PV_ZNAM="Invalid")
end_digitals()

add_digital("HardWItlHealthySt",             PV_NAME="ITLck:HW:HltyR",                PV_DESC="Hardware interlock is HEALTHY",           PV_ONAM="HEALTHY")
add_digital("HardwareInterlockSt",           PV_NAME="ITLck:HW:TrpR",                 PV_DESC="Hardware interlock is TRIPPED",           PV_ONAM="TRIPPED", PV_OSV="MAJOR")
add_digital("HardWItlBypassSt",              PV_NAME="ITLck:HW:OvRidnR",              PV_DESC="Hardware interlock is OVERRIDEN",         PV_ONAM="OVERRIDEN")
add_digital("Disable Hardware Interlock",    PV_NAME="ITLck:HW:DisR",                 PV_DESC="Hardware interlock is NOT CONFIGURED",    PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
add_digital("PressItlHealthySt",             PV_NAME="ITLck:Prs:HltyR",               PV_DESC="Pressure interlock is HEALTHY",           PV_ONAM="HEALTHY")
add_digital("PressureInterlockSt",           PV_NAME="ITLck:Prs:TrpR",                PV_DESC="Pressure interlock is TRIPPED",           PV_ONAM="TRIPPED", PV_OSV="MAJOR")
add_digital("PressItlBypassSt",              PV_NAME="ITLck:Prs:OvRidnR",             PV_DESC="Pressure interlock is OVERRIDEN",         PV_ONAM="OVERRIDEN")
add_digital("Disable Pressure Interlock",    PV_NAME="ITLck:Prs:DisR",                PV_DESC="Pressure interlock is NOT CONFIGURED",    PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
#-----------------------------
skip_digitals(4)
add_digital("SoftWItlHealthySt",             PV_NAME="ITLck:SW:HltyR",                PV_DESC="Software interlock is HEALTHY",           PV_ONAM="HEALTHY")
add_digital("SoftwareInterlockSt",           PV_NAME="ITLck:SW:TrpR",                 PV_DESC="Software interlock is TRIPPED",           PV_ONAM="TRIPPED", PV_OSV="MAJOR")
add_digital("SoftWItlBypassSt",              PV_NAME="ITLck:SW:OvRidnR",              PV_DESC="Software interlock is OVERRIDEN",         PV_ONAM="OVERRIDEN")
add_digital("Disable Software Interlock",    PV_NAME="ITLck:SW:DisR",                 PV_DESC="Software interlock is NOT CONFIGURED",    PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")

add_analog("WarningCode",          "BYTE",   PV_NAME="WarningCodeR",                  PV_DESC="Active warning code")

add_analog("ErrorCode",            "BYTE",   PV_NAME="ErrorCodeR",                    PV_DESC="Active error code", ARCHIVE=True)

add_analog("RunTime",              "REAL",   PV_NAME="RunTimeR",                      PV_DESC="Runtime in hours", PV_EGU="h", ARCHIVE=True)

add_analog("StartCounter",         "UDINT",  PV_NAME="StartCounterR",                 PV_DESC="Number of starts", ARCHIVE=True)

add_verbatim("""
record(calc, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:iTrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:ITLck:Prs:iTrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:Prs:TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:Prs:OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:iTrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:iHltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:DisR CP")
	field(CALC, "A || B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:ITLck:Prs:iHltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:Prs:HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:Prs:DisR CP")
	field(CALC, "A || B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:iHltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:DisR CP")
	field(CALC, "A || B")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:ITLck:iHltyR")
{
field(SCAN, "1 second")
	field(DESC, "Interlocks are HEALTHY")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:DisR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:iHltyR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck:Prs:iHltyR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:iHltyR CP")

	field(CALC, "!A && B && C && D")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck:HltyR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck:HltyR")
{
	field(DESC, "Interlocks are HEALTHY")
	field(ONAM, "HEALTHY")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:ITLck:iTrpR")
{
field(SCAN, "1 second")
	field(DESC, "At least one interlock is TRIPPED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:iTrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:Prs:iTrpR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:iTrpR CP")

	field(CALC, "A || B || C")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck:TrpR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck:TrpR")
{
	field(DESC, "At least one interlock is TRIPPED")
	field(OSV,  "MAJOR")
	field(ONAM, "TRIPPED")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:ITLck:iOvRidnR")
{
field(SCAN, "1 second")
	field(DESC, "At least one interlock is OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:OvRidnR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck:Prs:OvRidnR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:OvRidnR CP")

	field(CALC, "!A && (B || C || D)")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck:OvRidnR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck:OvRidnR")
{
	field(DESC, "At least one interlock is OVERRIDEN")
	field(ONAM, "OVERRIDEN")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:ITLck:iDisR")
{
field(SCAN, "1 second")
	field(DESC, "Interlocks are NOT CONFIGURED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:DisR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:Prs:DisR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:DisR CP")

	field(CALC, "A && B && C")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck:DisR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck:DisR")
{
	field(DESC, "Interlocks are NOT CONFIGURED")
	field(ONAM, "NOT CONFIGURED")
	field(ZNAM, "CONFIGURED")
	field(DISP, "1")
}


record(calcout, "[PLCF#INSTALLATION_SLOT]:iITLckStatR")
{
field(SCAN, "1 second")
	field(DESC, "Calculate combined interlock status")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:OvRidnR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck:HltyR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck:DisR CP")

# Check that only one is true
	field(CALC, "E:=(A | B*2 | C*4 | D*8);F:=!(E&(E-1));F")
	field(OCAL, "F?(C + A*2 + B*3 + D*4):0")
	field(DOPT, "Use OCAL")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLckStatR PP")
}
record(mbbi, "[PLCF#INSTALLATION_SLOT]:ITLckStatR")
{
	field(DESC, "Combined Interlock status")
	field(ZRST, "INVALID")
	field(ONST, "HEALTHY")
	field(TWST, "TRIPPED")
	field(TWSV, "MAJOR")
	field(THST, "OVERRIDEN")
	field(FRST, "NOT CONFIGURED")
}
""")

add_verbatim("""
record(calcout, "[PLCF#INSTALLATION_SLOT]:iStatR")
{
field(SCAN, "1 second")
	field(DESC, "Calculate pump status")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:AcceleratingR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:AtNominalSpdR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:StoppedR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ErrorR CP")
	field(INPE, "[PLCF#INSTALLATION_SLOT]:ValidR CP MSS")

# Check that only one is true. Error is a bit special though
	field(CALC, "F:=(A | B*2 | C*4 | D*8);G:=!(F&(F-1));G")
	field(OCAL, "E?(G?(A + B*2 + C*3 + D*4):D*4):0")
	field(DOPT, "Use OCAL")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:StatR PP MSS")
}
record(mbbi, "[PLCF#INSTALLATION_SLOT]:StatR")
{
	field(DESC, "Pump status")
	field(ZRST, "INVALID")
	field(ONST, "ACCELERATING")
	field(TWST, "AT NOMINAL SPEED")
	field(THST, "STOPPED")
	field(FRST, "ERROR")
	field(FRSV, "MAJOR")
}
""")


#-############################
#- METADATA
#-############################
#- ERRORS
define_metadata("error", MD_SCRIPT='css_code2msg.py', CODE_TYPE='error')
#-
add_metadata(99, 'Controller Error (Hardware Error)')
add_metadata(98, 'Pressure Interlock')
add_metadata(97, 'Hardware Interlock')
add_metadata(96, 'Software Interlock')
add_metadata(49, 'Controller Error (Hardware Error) - Auto Reset')
add_metadata(48, 'Pressure Interlock - Auto Reset')
add_metadata(47, 'Hardware Interlock - Auto Reset')
add_metadata(46, 'Software Interlock - Auto Reset')
#-
#- WARNINGS
define_metadata("warning", MD_SCRIPT='css_code2msg.py', CODE_TYPE='warning')
#-
add_metadata(99, 'Bypass Interlock Activated')
add_metadata(98, 'Pump Starting Prevented by Tripped Interlock')
add_metadata( 5, 'Pressure Interlock Bypassed')
add_metadata( 4, 'Hardware Interlock Bypassed')
add_metadata( 3, 'Software Interlock Bypassed')
